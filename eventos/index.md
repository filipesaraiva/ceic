---
title: Eventos
layout: default
---
# Eventos

A CEIC co-organiza juntamente com a CEIA o __Brazilian Conference on Intelligent System (BRACIS)__ e o __Encontro Nacional de Inteligência Artificial e Computacional (ENIAC)__, principais eventos da área realizados no Brasil.

Além destes, a comissão também promove o __Concurso de Teses e Dissertações em Inteligência Artificial e Computacional (CTDIAC)__, que premia os melhores trabalhos da área produzidos pelos programas de pós graduação brasileiros.

Saiba mais sobre cada evento nas páginas à seguir:

* [BRACIS - Brazilian Conference on Intelligent System](bracis/)
* [ENIAC - Encontro Nacional de Inteligência Artificial e Computacional](eniac/)
* [CTDIAC - Concurso de Teses e Dissertações em Inteligência Artificial e Computacional](ctdiac/)
