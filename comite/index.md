---
title: Comitê
layout: default
---
# Comitê

A CEIC é dirigida por um coordenador juntamente com um Comitê Assessor (_Steering Committee_) eleitos em assembleia realizada durante o BRACIS para um mandato de dois anos.

Cabe ao coordenador representar a CEIC perante a SBC e ao Comitê Assessor traçar as diretrizes para os próximos eventos e atividades da comissão.

O Comitê Assessor é definido quando da realização da assembleia, e tem a seguinte composição:

* Coordenador do último evento (presidente do Comitê Assessor);
* Coordenador de programa do último evento;
* Coordenador geral do próximo evento;
* Último coordenador da CEIC;
* Um membro eleito durante a realização da Assembleia.

O coordenador da CEIC será também escolhido por votação durante a assembleia. Por tradição, o coordenador geral do último evento é o candidato natural.

## Gestões
<div class="accordion" id="accordion">
    <div class="card">
        <div class="card-header" id="headingOne">
        <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Gestão 2018-2020
            </button>
        </h2>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
            <h4>Coordenação</h4>
            <p>Ricardo Bastos C. Prudencio (UFPE)</p>
            <h4>Comitê Assessor</h4>
            <ul>
                <li>Anne Magaly de Paula Canuto (UFRN)</li>
                <li>Anisio Lacerda (CEFET e UFMG)</li>
                <li>Gina Maira Barbosa de Oliveira (UFU)</li>
                <li>Gisele Pappa (UFMG)</li>
                <li>Myriam Delgado (UTFPR)</li>
                <li>Renato Tinós (USP)</li>
                <li>Paulo Rodrigo Cavalin (IBM)</li>
            </ul>
        </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
        <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Gestão 2015-2017
            </button>
        </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">
            <h4>Coordenação</h4>
            <p>Anne Magaly de Paula Canuto (UFRN)</p>
            <h4>Comitê Assessor</h4>
            <ul>
                <li>Andre Carlos de Carvalho (USP)</li>
                <li>Aurora Pozo (UFPR)</li>
                <li>Gisele Pappa (UFMG)</li>
                <li>Myriam Delgado (UTFPR)</li>
                <li>Paulo Rodrigo Cavalin (IBM Research)</li>
                <li>Ricardo Prudêncio (UFPE)</li>
            </ul>
        </div>
        </div>
    </div>
</div>
