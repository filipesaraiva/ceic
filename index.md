---
title: Sociedade Brasileira de Computação
layout: default
---

# Comissão Especial de Inteligência Computacional

A __Comissão Especial de Inteligência Computacional (CEIC)__ reúne o grupo de associadas e associados da [Sociedade Brasileira de Computação (SBC)](https://www.sbc.org.br) com interesse em aspectos teóricos e práticos de técnicas inteligentes tais como:

* Aprendizado de Máquina;
* Computação Evolucionária;
* Sistemas Fuzzy;
* Sistemas Híbridos;

E demais sub-áreas da Inteligência Computacional.

A CEIC co-organiza juntamente com a CEIA o [Brazilian Conference on Intelligent System (BRACIS)](/eventos/bracis) e o [Encontro Nacional de Inteligência Artificial e Computacional (ENIAC)](/eventos/eniac), principais eventos da área realizados no Brasil.

Além destes, a comissão também promove o [Concurso de Teses e Dissertações em Inteligência Artificial e Computacional (CTDIAC)](/eventos/ctdiac), que premia os melhores trabalhos da área produzidos pelos programas de pós graduação brasileiros.
